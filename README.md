# MAJIQ Paper Versions

Copyright Biociphers Lab, University of Pennsylvania.

License: MAJIQ has a dual [academic/nonprofit][majiq-academic] vs
[commercial][majiq-commercial] license that must be accepted before use. Please
visit and accept the license pertinent for your use case.

   [majiq-academic]: https://majiq.biociphers.org/app_download/
   [majiq-commercial]: https://majiq.biociphers.org/commercial.php

This repository has orphan branches for different versions of MAJIQ with
features necessary to reproduce work done in papers that have not yet fully
been integrated into MAJIQ stable.

Please see branches:

+ [`majiq-for-cats`](https://bitbucket.org/jkaicher/majiq-paper-versions/src/majiq-for-cats/):
  Version of MAJIQ required for Aicher et al "Mapping RNA splicing variations
  in clinically-accessible and non-accessible tissues to facilitate Mendelian
  disease diagnosis using RNA-seq". We provide the conda environment file
  [`majiq-for-cats.yaml`](majiq-for-cats.yaml) to facilitate intallation with
  conda (and Snakemake workflows using conda).


## Installation

Installation requires a compiled version of htslib (>= 1.9) with OpenMP
support during compilation. This can either be installed in a default
system-wide location (i.e. `/usr/local/{lib,include}`) or to an alternative
location specified by the environment variables `HTSLIB_LIBRARY_DIR` and
`HTSLIB_INCLUDE_DIR`.

An installation of htslib 1.9 to a directory `${USER_DIR}` can be done, for
example, by running:

```
# download htslib 1.9 source code
wget https://github.com/samtools/htslib/releases/download/1.9/htslib-1.9.tar.bz2
# extract archive of source code and move into source directory
tar -xvjf htslib-1.9.tar.bz2
cd htslib-1.9
# configure, compile, and install htslib to ${USER_DIR} for your system
./configure --prefix=${USER_DIR}
make
make install
```

Then, you can install the version of majiq specified by the configuration file
`${MAJIQ_ENV_YAML}` (e.g. `majiq-for-cats.yaml`) by setting the environment
variables and importing the conda environment:

```
# set up environment variables for your current session
export HTSLIB_LIBRARY_DIR=${USER_DIR}/lib
export HTSLIB_INCLUDE_DIR=${USER_DIR}/include
# install majiq to environment named ${NEW_ENV_NAME}
conda env create -f ${MAJIQ_ENV_YAML} -n ${NEW_ENV_NAME}
```

You can then run `conda activate ${NEW_ENV_NAME}` to activate the new virtual
environment with MAJIQ.

For Snakemake installation, install htslib and set up the environment variables
for the same session that snakemake will run in, and place the YAML
configuration file in the location specified by the Snakemake workflow (e.g.
`rules/envs`). Running the Snakemake workflow will then be able to install and
manage a majiq-specific environment as needed.
